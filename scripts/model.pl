#!/usr/bin/perl -w

use strict;

open(FH, $ARGV[0]);

while ( my $line = <FH> ) {
	
	chomp $line;

	if ( $line =~ /^\s*([0-9,\.\-]+)\s+([0-9,\.\-]+)\s+([0-9,\.\-]+)/ ) {

		my $x = $1; my $y = $2; my $z = $3;
		
		printf("%8.2f%8.2f%8.2f\n", $x/100.0, $y/100.0, $z/100.0);
		
	} else {
		printf("%s\n", $line);
	}
	
}

close(FH);

exit(0);

