#!/bin/sh

rm -f /usr/local/share/thrust3d/shaders/lighting.vert
ln -s $(pwd)/data/shaders/lighting.vert /usr/local/share/thrust3d/shaders/lighting.vert

rm -f /usr/local/share/thrust3d/shaders/lighting.frag
ln -s $(pwd)/data/shaders/lighting.frag /usr/local/share/thrust3d/shaders/lighting.frag

rm -f /usr/local/share/thrust3d/shaders/fill-light.vert
ln -s $(pwd)/data/shaders/fill-light.vert /usr/local/share/thrust3d/shaders/fill-light.vert

rm -f /usr/local/share/thrust3d/shaders/fill-light.frag
ln -s $(pwd)/data/shaders/fill-light.frag /usr/local/share/thrust3d/shaders/fill-light.frag

rm -f /usr/local/share/thrust3d/shaders/swirlytron.vert
ln -s $(pwd)/data/shaders/swirlytron.vert /usr/local/share/thrust3d/shaders/swirlytron.vert

rm -f /usr/local/share/thrust3d/shaders/swirlytron.frag
ln -s $(pwd)/data/shaders/swirlytron.frag /usr/local/share/thrust3d/shaders/swirlytron.frag

