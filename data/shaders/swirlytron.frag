/*
 * swirlytron.frag
 *
 * Swirly stuff for rendering onto swirly things...
 *
 * (c) 2007-2008 Thomas White <taw27@cam.ac.uk>
 *
 *  thrust3d - a silly game
 *
 */

varying vec2 coords;
varying vec2 incoords;
uniform float time;
uniform float rrb;		/* Recharge ripple brightness */
uniform bool rechargeripple;

void main() {
	
	vec3 colour;
	
	colour.r = 0.0;
	colour.g = 0.0;
	colour.b = 0.5*max(0.0, cos(2.0*3.141*coords.x-(time/250.0))) + 0.5*max(0.0, cos(2.0*3.141*coords.y-(time/250.0)));

	if ( rechargeripple ) {
		
		const float f = 1.0;
		float r;
		
		r = pow(abs(incoords.x), 2.0) + pow(abs(incoords.y), 2.0);
		
		colour.r += rrb * 0.8 * cos(2.0*f*3.141*r + (time/250.0) );
		colour.g += rrb * 0.3 * cos(2.0*f*3.141*r + (time/250.0) );
		colour.b += rrb * 0.1;
		
	}
	
	gl_FragColor = vec4(colour.r, colour.g, colour.b, 1.0);
	
}

