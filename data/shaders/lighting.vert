/*
 * lighting.vert
 *
 * Lighting calculations
 *
 * (c) 2007-2008 Thomas White <taw27@cam.ac.uk>
 *
 *  thrust3d - a silly game
 *
 */

varying vec3 pos;
varying vec3 normal;

varying vec3 light0vc;
varying vec3 light1vc;

varying vec3 col_ambi_diff;
varying vec3 col_emit;
varying float col_spec;	/* Only use one component of this, assume it's some shade of grey */

void main() {
	
	vec4 vert;
	
	vert = gl_ModelViewMatrix * gl_Vertex;
	pos = vert.xyz;
	normal = gl_NormalMatrix * gl_Normal;
	
	/* Light 0: Lander craft's spotlight */
	light0vc = gl_LightSource[0].position.xyz - vert.xyz;	/* Don't normalise here */
	
	/* Light 1: Diffuse background glow */
	light1vc = normalize(vec3(gl_LightSource[1].position));
	
	/* Material properties */
	col_ambi_diff = gl_Color.rgb;
	col_spec = gl_FrontMaterial.specular.r;
	col_emit = gl_FrontMaterial.emission.rgb;
	
	/* Coordinates */
	gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;
	gl_Position = ftransform();
	
}

