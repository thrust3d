/*
 * fill-light.vert
 *
 * Simplified lighting calculations for the lander craft
 *
 * (c) 2007-2008 Thomas White <taw27@cam.ac.uk>
 *
 *  thrust3d - a silly game
 *
 */

varying vec3 pos;
varying vec3 normal;

varying vec3 light2vc;

varying vec3 col_ambi_diff;
varying vec3 col_emit;
varying float col_spec;

void main() {
	
	vec4 vert;
	
	vert = gl_ModelViewMatrix * gl_Vertex;
	pos = vert.xyz;
	normal = gl_NormalMatrix * gl_Normal;
	
	/* Light 2: Fill-in light for lander */
	light2vc = normalize(vec3(gl_LightSource[2].position));
	
	/* Material properties */
	col_ambi_diff = gl_Color.rgb;
	col_emit = gl_FrontMaterial.emission.rgb;
	col_spec = gl_FrontMaterial.specular.r;
	
	/* Coordinates */
	gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;
	gl_Position = ftransform();
	
}

