/*
 * lighting.frag
 *
 * Lighting calculations
 *
 * (c) 2007-2008 Thomas White <taw27@cam.ac.uk>
 *
 *  thrust3d - a silly game
 *
 */

varying vec3 pos;
varying vec3 normal;

varying vec3 light0vc;
varying vec3 light1vc;

varying vec3 col_ambi_diff;
varying vec3 col_emit;
varying float col_spec;

uniform sampler2D texture;
uniform sampler2D normalmap;

uniform bool has_normals;
uniform bool texture_emits;
uniform float alpha;
uniform float shininess;

void main() {
	
	vec3 ambi, emit, diff, spec;
	vec3 norm;
	float falloff, spot;
	float diff_fac, spec_fac;
	vec3 L, E, R;
	vec3 tex;
	
	diff = vec3(0.0, 0.0, 0.0);
	spec = vec3(0.0, 0.0, 0.0);
	
	norm = normalize(normal);
	if ( has_normals ) {
		/* Same texture coordinates as for the diffuse map */
		norm += (texture2D(normalmap, gl_TexCoord[0].st).rgb - vec3(0.5, 0.5, 0.5)) / 2.0;
	}
	
	/* Emission */
	emit = col_emit;
	
	/* Light 0: Lander craft's spotlight */
	falloff = 1.0 / (  gl_LightSource[0].constantAttenuation
		            + gl_LightSource[0].linearAttenuation * length(light0vc)
		            + gl_LightSource[0].quadraticAttenuation * pow(length(light0vc), 2.0) );
	
	spot = max(dot(normalize(-light0vc), gl_LightSource[0].spotDirection), 0.0);
	spot = pow(spot, gl_LightSource[0].spotExponent);
	
	/* Ambient */
	ambi = col_ambi_diff * (gl_LightModel.ambient.rgb + gl_LightSource[0].ambient.rgb * spot * falloff);
	
	/* Diffuse */
	diff_fac = max(0.0, dot(normalize(light0vc).xyz, norm));
	diff += col_ambi_diff * gl_LightSource[0].diffuse.rgb * spot * falloff * diff_fac;
	
	/* Specular */
	L = normalize(gl_LightSource[0].position.xyz - pos); 
	E = normalize(-pos);
	R = normalize(-reflect(L, norm)); 
	spec_fac = max(0.0, dot(R, E));
	spec_fac = pow(spec_fac, shininess);
	spec += col_spec * gl_LightSource[0].specular.rgb * spot * falloff * spec_fac;
	
	/* Light 1: Diffuse background glow */
	diff += col_ambi_diff * gl_LightSource[1].diffuse.rgb * max(0.0, dot(vec3(light1vc), norm));
	
	tex = texture2D(texture, gl_TexCoord[0].st).rgb;
	if ( texture_emits ) {
		gl_FragColor = vec4(tex.r, tex.g, tex.b, alpha);
	} else {
		gl_FragColor = vec4(min(tex.r * (ambi.r + diff.r) + spec.r, 1.0),
				    min(tex.g * (ambi.g + diff.g) + spec.g, 1.0),
				    min(tex.b * (ambi.b + diff.b) + spec.b, 1.0),
				    alpha);
	}
	
}

