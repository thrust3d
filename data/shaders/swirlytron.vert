/*
 * swirlytron.vert
 *
 * Swirly stuff for rendering onto swirly things...
 *
 * (c) 2007-2008 Thomas White <taw27@cam.ac.uk>
 *
 *  thrust3d - a silly game
 *
 */

varying vec2 coords;
varying vec2 incoords;
uniform vec2 lander;

void main() {
	
	coords = gl_Vertex.xy;
	incoords = gl_Vertex.xy + lander;
	
	/* Coordinates */
	gl_TexCoord[0] = gl_TextureMatrix[0] * gl_MultiTexCoord0;
	gl_Position = ftransform();
	
}

