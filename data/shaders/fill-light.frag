/*
 * fill-light.frag
 *
 * Simplified lighting calculations for the lander craft
 *
 * (c) 2007-2008 Thomas White <taw27@cam.ac.uk>
 *
 *  thrust3d - a silly game
 *
 */

varying vec3 pos;
varying vec3 normal;

varying vec3 light2vc;

uniform sampler2D texture;
uniform sampler2D normalmap;

uniform bool has_normals;
uniform float alpha;
uniform float shininess;

varying vec3 col_ambi_diff;
varying vec3 col_emit;
varying float col_spec;

void main() {
	
	vec3 ambi, emit, diff, spec;
	vec3 norm;
	float diff_fac, spec_fac;
	vec3 L, E, R;
	vec3 tex;
	
	tex = texture2D(texture, gl_TexCoord[0].st).rgb;
	
	if ( has_normals ) {
		norm += (texture2D(normalmap, gl_TexCoord[0].st).rgb - vec3(0.5, 0.5, 0.5)) / 2.0;
	}
	
	norm = normalize(normal);
	
	/* Ambient */
	ambi = col_ambi_diff * (gl_LightModel.ambient.rgb + vec3(0.2, 0.2, 0.2));
	
	/* Emission */
	emit = col_emit;
	
	/* Light 2: Fill-in for lander craft */
	L = normalize(gl_LightSource[2].position.xyz - pos); 
	E = normalize(-pos);
	R = normalize(-reflect(L, norm)); 
	diff_fac = max(0.0, dot(normalize(light2vc).xyz, norm));
	spec_fac = max(0.0, dot(R, E));
	spec_fac = pow(spec_fac, shininess);
	
	diff = col_ambi_diff * gl_LightSource[2].diffuse.rgb * diff_fac;
	spec = gl_LightSource[2].specular.rgb * spec_fac * col_spec;
	
	gl_FragColor = vec4(min(tex.r * (ambi.r + diff.r) + spec.r, 1.0),
			    min(tex.g * (ambi.g + diff.g) + spec.g, 1.0),
			    min(tex.b * (ambi.b + diff.b) + spec.b, 1.0),
			    alpha);

}

