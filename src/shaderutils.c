/*
 * shaderuitils.c
 *
 * Shader utilities
 *
 * Copyright (c) 2008 Thomas White <taw27@cam.ac.uk>
 *
 * This file is part of Thrust3D - a silly game
 *
 * Thrust3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Thrust3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrust3D.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glew.h>
#include <stdio.h>

void shaderutils_setunf(GLuint program, const char *name, GLfloat val) {
	GLint loc;
	loc = glGetUniformLocation(program, name);
	glUniform1f(loc, val);
}

void shaderutils_setun2f(GLuint program, const char *name, GLfloat val1, GLfloat val2) {
	GLint loc;
	loc = glGetUniformLocation(program, name);
	glUniform2f(loc, val1, val2);
}

void shaderutils_setuni(GLuint program, const char *name, GLint val) {
	GLint loc;
	loc = glGetUniformLocation(program, name);
	glUniform1i(loc, val);
}

GLuint shaderutils_load_shader(const char *filename, GLenum type) {

	GLuint shader;
	char text[4096];
	size_t len;
	FILE *fh;
	int l;
	GLint status;
	
	fh = fopen(filename, "r");
	if ( fh == NULL ) {
		fprintf(stderr, "Couldn't load shader '%s'\n", filename);
		return 0;
	}
	len = fread(text, 1, 4095, fh);
	fclose(fh);
	text[len] = '\0';
	const GLchar *source = text;
	shader = glCreateShader(type);
	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status); 
	if ( status == GL_FALSE ) {
		printf("Problems loading '%s':\n", filename);
		glGetShaderInfoLog(shader, 4095, &l, text);
		if ( l > 0 ) {
			printf("%s\n", text); fflush(stdout);
		} else {
			printf("Shader compilation failed.\n");
		}
	}
	
	return shader;
	
}

int shaderutils_link_program(GLuint program) {

	int l;
	GLint status;
	char text[4096];
	
	glLinkProgram(program);
	
	glGetProgramiv(program, GL_LINK_STATUS, &status); 
	if ( status == GL_FALSE ) {
		printf("Program linking errors:\n");
		glGetProgramInfoLog(program, 4095, &l, text);
		if ( l > 0 ) {
			printf("%s\n", text); fflush(stdout);
		} else {
			printf("Program linking failed.\n");
		}
	}
	
	return status;
	
}

int shaderutils_validate_program(GLuint program) {
	
	GLint status;
	int l;
	char text[4096];
	
	glValidateProgram(program);
	glGetProgramiv(program, GL_VALIDATE_STATUS, &status); 
	if ( status == GL_FALSE ) {
		printf("Program validation errors:\n");
		glGetProgramInfoLog(program, 4095, &l, text);
		if ( l > 0 ) {
			printf("%s\n", text); fflush(stdout);
		} else {
			printf("Program did not validate successfully.\n");
		}
		return 0;
	}
	
	return 1;
	
}

