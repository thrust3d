/*
 * obj2model.c
 *
 * Turn Wavefront OBJ files into Thrust3D models
 *
 * Copyright (c) 2008 Thomas White <taw27@cam.ac.uk>
 *
 * This file is part of Thrust3D - a silly game
 *
 * Thrust3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Thrust3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrust3D.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <libgen.h>

#include "utils.c"

#define MAX_VERTICES 65536

static char *texname(const char *filename) {
	int i;
	char *rval;
	rval = strdup(filename);
	for ( i=0; i<strlen(rval); i++ ) {
		if ( rval[i] == '.' ) {
			rval[i] = '\0';
			return rval;
		}
	}
	return rval;
}

static void do_material(const char *mtllib, const char *mtlname, FILE *out, char *infilename) {

	FILE *fh;
	char tmp[1024];
	char *infilename_dup;
	
	infilename_dup = strdup(infilename);
	snprintf(tmp, 1023, "%s/%s", dirname(infilename_dup), mtllib);
	free(infilename_dup);
	fh = fopen(tmp, "r");
	if ( fh == NULL ) {
		fprintf(stderr, "Couldn't open material library '%s'\n", tmp);
		return;
	}
	
	while ( !feof(fh) ) {
		
		char line[1024];
		char **bits;
		int n, i;
		
		fgets(line, 1023, fh);
		n = assplode(line, " \t\r\n", &bits, ASSPLODE_NONE);
		
		if ( n > 1 ) {
			if ( (strcmp(bits[0], "newmtl") == 0) && (strcmp(bits[1], mtlname) == 0) ) {
				/* Found the right material */
				while ( !feof(fh) ) {
		
					char line[1024];
					char **bits;
					int n, i;
		
					fgets(line, 1023, fh);
					n = assplode(line, " \t\r\n", &bits, ASSPLODE_NONE);
		
					if ( n > 1 ) {
						
						if ( strcmp(bits[0], "newmtl") == 0 ) {
							return;
						}
						if ( strcmp(bits[0], "Kd") == 0 ) {
							fprintf(out, "colour %s %s %s\n", bits[1], bits[2], bits[3]);
						}
						if ( strcmp(bits[0], "Ks") == 0 ) {
							fprintf(out, "colspec %s %s %s\n", bits[1], bits[2], bits[3]);
						}
						if ( strcmp(bits[0], "map_Kd") == 0 ) {
							char *tex;
							tex = texname(bits[1]);
							fprintf(out, "texture %s\n", tex);
							free(tex);
						}
						if ( strcmp(bits[0], "Ns") == 0 ) {
							fprintf(out, "shiny %s\n", bits[1]);
						}
					}
		
					for ( i=0; i<n; i++ ) free(bits[i]);
					free(bits);
		
				}
	
			}
		}
		
		for ( i=0; i<n; i++ ) free(bits[i]);
		free(bits);
		
	}
	
	fclose(fh);
	
}

int main(int argc, char *argv[]) {

	FILE *fh;
	FILE *out;
	float vtmp[3*MAX_VERTICES];
	float vntmp[3*MAX_VERTICES];
	float textmp[2*MAX_VERTICES];
	int v_used[MAX_VERTICES];
	int vn_used[MAX_VERTICES];
	int tex_used[MAX_VERTICES];
	int n_vtmp, n_vntmp, n_textmp;
	int new_prim, n_prev;
	int i, v_unused, vn_unused, tex_unused;
	char mtllib[64];
	char *cur_material = NULL;
	
	fh = fopen(argv[1], "r");
	if ( fh == NULL ) {
		fprintf(stderr, "Couldn't open '%s'\n", argv[1]);
		return 1;
	}
	
	for ( i=0; i<MAX_VERTICES; i++ ) {
		v_used[i] = 5;
		vn_used[i] = 5;
		tex_used[i] = 5;
	}
	
	/* Zip through and find all the vertices */
	n_vtmp = 0;
	n_textmp = 0;
	n_vntmp = 0;
	while ( !feof(fh) ) {
		
		char line[1024];
		float x, y, z;
		size_t s;
		
		fgets(line, 1023, fh);
		s = 0;
		for ( ; s<strlen(line); s++ ) {
			if ( line[s] != ' ' ) break;
		}
			
		if ( line[s] == '#' ) {
			continue;
		}
		
		if ( sscanf(line+s, "v %f %f %f\n", &x, &y, &z) == 3 ) {
			vtmp[3*n_vtmp+0] = x;
			vtmp[3*n_vtmp+1] = y;
			vtmp[3*n_vtmp+2] = z;
			v_used[n_vtmp] = 0;
			n_vtmp++;
			continue;
		}
		
		if ( sscanf(line+s, "vn %f %f %f\n", &x, &y, &z) == 3 ) {
			vntmp[3*n_vntmp+0] = x;
			vntmp[3*n_vntmp+1] = y;
			vntmp[3*n_vntmp+2] = z;
			vn_used[n_vntmp] = 0;
			n_vntmp++;
			continue;
		}
		
		if ( sscanf(line+s, "vt %f %f %f\n", &x, &y, &z) == 3 ) {
			textmp[2*n_textmp+0] = x;
			textmp[2*n_textmp+1] = y;
			tex_used[n_textmp] = 0;
			n_textmp++;
			continue;
		}
		
		if ( sscanf(line+s, "mtllib %63s\n", mtllib) == 1 ) {
			printf("Material library '%s'\n", mtllib);
			continue;
		}
	
	}
	
	/* Go through again and look for faces */
	rewind(fh);
	out = fopen(argv[2], "w");
	fprintf(out, "# %s\n", argv[1]);
	fprintf(out, "\n");
	new_prim = 1;
	n_prev = 0;
	while ( !feof(fh) ) {
	
		char line[1024];
		char **bits;
		int n, i;
		
		fgets(line, 1023, fh);
		n = assplode(line, " \t\r\n", &bits, ASSPLODE_NONE);
		
		/* Read in a face */
		if ( strcmp(bits[0], "f") == 0 ) {
			
			int n_this = n-1;
			if ( new_prim || (n_this != n_prev) ) {
				if ( n_this != n_prev ) {
					fprintf(out, "\n");
				}
				if ( n_this == 4 ) {
					fprintf(out, "QUADS\n");
				} else if ( n_this == 3 ) {
					fprintf(out, "TRIANGLES\n");
				} else {
					fprintf(out, "POLYGONS\n");
				}
				new_prim = 0;
				n_prev = n_this;
				if ( cur_material != NULL ) {
					do_material(mtllib, cur_material, out, argv[1]);
				}
			}
			
			/* For each vertex... */
			for ( i=1; i<n; i++ ) {
			
				char **sp;
				int np, j, nslash;
				
				nslash = 0;
				for ( j=0; j<strlen(bits[i]); j++ ) {
					if ( bits[i][j] == '/' ) nslash++;
				}
				if ( nslash == 2 ) {
				
					int vnum, tnum, nnum;
				
					np = assplode(bits[i], "/", &sp, ASSPLODE_DUPS);
					if ( np != 3 ) {
						printf("Error!\n");
						continue;
					}
					vnum = atoi(sp[0])-1;
					tnum = atoi(sp[1])-1;
					nnum = atoi(sp[2])-1;
					if ( vnum >= n_vtmp ) {
						fprintf(stderr, "Vertex index is too high (%i/%i)\n", vnum, n_vtmp);
						continue;
					}
					if ( nnum >= n_vntmp ) {
						fprintf(stderr, "Normal index is too high (%i/%i)\n", nnum, n_vntmp);
						continue;
					}
					fprintf(out, "%+8.3f %+8.3f %+8.3f      %8.3f %8.3f     %+8.3f %+8.3f %+8.3f\n",
							vtmp[3*vnum+0], vtmp[3*vnum+1], vtmp[3*vnum+2],
							textmp[2*tnum+0], textmp[2*tnum+1],
							vntmp[3*nnum+0], vntmp[3*nnum+1], vntmp[3*nnum+2]);
					v_used[vnum] = 1;
					vn_used[nnum] = 1;
					tex_used[tnum] = 1;
					free(sp[0]);
					free(sp[1]);
					free(sp[2]);
					free(sp);
				
				} else if ( nslash == 0 ) {
					
					int vnum;
					vnum = atoi(bits[i])-1;
					fprintf(out, "%+8.3f %+8.3f %+8.3f      %8.3f %8.3f\n",
							vtmp[3*vnum+0], vtmp[3*vnum+1], vtmp[3*vnum+2],
							0.0, 0.0);
					v_used[vnum] = 1;
				
				}
				
			}
			
		} else if ( strcmp(bits[0], "usemtl") == 0 ) {
			cur_material = strdup(bits[1]);
		} else if ( strcmp(bits[0], "v") == 0 ) {
			if ( !new_prim ) {
				fprintf(out, "\n");
			}
			new_prim = 1;
		}
		
		for ( i=0; i<n; i++ ) free(bits[i]);
		free(bits);
		
	}
	fprintf(out, "\n");
	
	fclose(fh);
	fclose(out);
	
	v_unused = 0;
	vn_unused = 0;
	tex_unused = 0;
	for ( i=0; i<MAX_VERTICES; i++ ) {
		if ( v_used[i] == 0 ) {
			v_unused++;
		}
		if ( vn_used[i] == 0 ) {
			vn_unused++;
		}
		if ( tex_used[i] == 0 ) {
			tex_unused++;
		}
	}
	printf("%i vertices (%i unused)\n", n_vtmp, v_unused);
	printf("%i normals (%i unused)\n", n_vntmp, vn_unused);
	printf("%i texcoords (%i unused)\n", n_textmp, tex_unused);
	
	return 0;
	
}

