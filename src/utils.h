/*
 * utils.h
 *
 * Utility stuff
 *
 * Copyright (c) 2008 Thomas White <taw27@cam.ac.uk>
 *
 * This file is part of Thrust3D - a silly game
 *
 * Thrust3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Thrust3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrust3D.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef UTILS_H
#define UTILS_H

#include <math.h>

#define rad2deg(a) ((a)*180/M_PI)
#define deg2rad(a) ((a)*M_PI/180)

typedef enum {
	ASSPLODE_NONE	= 0,
	ASSPLODE_DUPS	= 1<<0
} AssplodeFlag;

extern void chomp(char *a);
extern int assplode(const char *a, const char *delims, char ***pbits, AssplodeFlag flags);
extern double utils_highresms(void);

/* So you can do assert(this_point_not_reached) */
extern int this_point_not_reached;

#endif	/* UTILS_H */

