/*
 * audio.h
 *
 * Sound stuff
 *
 * Copyright (c) 2008 Thomas White <taw27@cam.ac.uk>
 *
 * This file is part of Thrust3D - a silly game
 *
 * Thrust3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Thrust3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrust3D.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef AUDIO_H
#define AUDIO_H

#include "types.h"

extern AudioContext *audio_setup(int debug, int no_music);
extern void audio_shutdown(AudioContext *ctx);
extern void audio_play(AudioContext *a, char *name, float volume, int repeat);
extern void audio_pause(AudioContext *a);
extern void audio_unpause(AudioContext *a);

#endif	/* AUDIO_H */

