/*
 * render-text.c
 *
 * Simple text rendering
 *
 * Copyright (c) 2008 Thomas White <taw27@cam.ac.uk>
 *
 * This file is part of Thrust3D - a silly game
 *
 * Thrust3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Thrust3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrust3D.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <png.h>
#include <stdlib.h>
#include <glew.h>

#include "render.h"
#include "texture.h"

void render_text_setup(RenderContext *r) {

	texture_load(r, "font");

}

void render_text_write(GLfloat x, GLfloat y, const char *text, RenderContext *r) {

	Texture *texture;
	int i;
	const GLfloat lwidth = 0.03;
	const GLfloat lheight = lwidth*r->aspect;
	const GLfloat lspacing = lwidth/10.0;
	
	y -= lheight;
	
	texture = texture_lookup(r, "font");
	glBindTexture(GL_TEXTURE_2D, texture->texname);
	glEnable(GL_TEXTURE_2D);
	glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	
	glBegin(GL_QUADS);
	
		for ( i=0; i<strlen(text); i++ ) {
		
			int idx;
			int tx, ty;
			GLfloat txc, tyc;
			const GLfloat unit = 1.0/8.0;
			char c;
			
			c = text[i];
			if ( c == ' ' ) {
				x += lwidth + lspacing;
				continue;
			}
			if ( c == '-' ) {
				c = 'Z' + 1;
			}
			if ( (c == '\r') || (c == '\n') ) {
				continue;
			}
			
			idx = toupper(c) - 'A';
			tx = idx % 8;  ty = idx / 8;
			txc = tx * unit;
			tyc = (1.0-unit) - ty * unit;
			
			glColor3f(0.4, 0.4, 0.4);
			glTexCoord2f(txc, tyc);
			glVertex2f(x, y);
			
			glTexCoord2f(txc+unit, tyc);
			glVertex2f(x+lwidth, y);
			
			glColor3f(0.8, 0.8, 0.8);
			glTexCoord2f(txc+unit, tyc+unit);
			glVertex2f(x+lwidth, y+lheight);
			
			glTexCoord2f(txc, tyc+unit);
			glVertex2f(x, y+lheight);
			
			x += lwidth + lspacing;
			
		}
		
	glEnd();
	
	glDisable(GL_TEXTURE_2D);

}

