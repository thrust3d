/*
 * render.h
 *
 * Render the scene
 *
 * Copyright (c) 2008 Thomas White <taw27@cam.ac.uk>
 *
 * This file is part of Thrust3D - a silly game
 *
 * Thrust3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Thrust3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrust3D.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef RENDER_H
#define RENDER_H

#include "types.h"

extern RenderContext *render_setup(int width, int height, int disable_vbos, int disable_fbos, int disable_shaders);
extern void render_shutdown(RenderContext *ctx);
extern void render_draw(Game *game);
extern void render_set_wireframe(int wireframe);

#endif	/* RENDER_H */

