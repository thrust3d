/*
 * shaderuitils.h
 *
 * Shader utilities
 *
 * Copyright (c) 2008 Thomas White <taw27@cam.ac.uk>
 *
 * This file is part of Thrust3D - a silly game
 *
 * Thrust3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Thrust3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrust3D.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <glew.h>

#ifndef SHADERUTILS_H
#define SHADERUTILS_H

extern void shaderutils_setunf(GLuint program, const char *name, GLfloat val);
extern void shaderutils_setun2f(GLuint program, const char *name, GLfloat val1, GLfloat val2);
extern void shaderutils_setuni(GLuint program, const char *name, GLint val);
extern GLuint shaderutils_load_shader(const char *filename, GLenum type);
extern int shaderutils_link_program(GLuint program);
extern int shaderutils_validate_program(GLuint program);

#endif	/* SHADERUTILS_H */

