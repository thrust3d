/*
 * types.h
 *
 * All the data types - avoid circular reference silliness
 *
 * Copyright (c) 2008 Thomas White <taw27@cam.ac.uk>
 *
 * This file is part of Thrust3D - a silly game
 *
 * Thrust3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Thrust3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrust3D.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef TYPES_H
#define TYPES_H

#include <gl.h>
#include <SDL.h>
#include <pthread.h>

/* Maximum number of individual textures */
#define MAX_TEXTURES 128

/* Maximum number of rooms which can be "seen" at once */
#define MAX_ROOMS 64

/* Maximum number of lights per room */
#define MAX_LIGHTS 4

typedef struct {

	int disable_vbos;
	int disable_fbos;
	int disable_shaders;
	int audio_debug;
	int game_debug;
	int no_music;
	int no_framerate_limit;
	int status_line;

} GameOptions;

typedef enum {
	ATTRIB_NONE	= 0,
	ATTRIB_COLOUR	= 1<<0,		/* Colour specified? */
	ATTRIB_PULSE	= 1<<1,		/* Pulsating colour */
	ATTRIB_SHINY	= 1<<2,		/* Primitive is shiny */
	ATTRIB_SWIRLY	= 1<<3,		/* Primitive is texture with swirlyness */
	ATTRIB_COLSPEC  = 1<<4,		/* Specular colour specified? */
} PrimitiveAttrib;

typedef enum {
	PRIMITIVE_QUADS,
	PRIMITIVE_TRIANGLES
} PrimitiveType;

typedef struct {

	GLenum type;
	int num_vertices;
	GLfloat *vertices;
	GLfloat *normals;
	GLfloat *texcoords;
	GLuint vertices_buffer;
	GLuint normals_buffer;
	GLuint texcoords_buffer;
	
	PrimitiveAttrib attribs;
	GLfloat col_r;
	GLfloat col_g;
	GLfloat col_b;
	GLfloat colspec;
	char *texture;
	GLfloat radius;
	GLfloat shininess;
	
} Primitive;

typedef struct {

	char *name;		/* Identifier */
	
	int num_primitives;
	Primitive **primitives;	/* Geometry */
	
	int num_coll_primitives;
	Primitive **coll_primitives;
	
	PrimitiveAttrib attrib_total;
	
} Model;

typedef struct {
	int num_models;
	Model **models;
} ModelContext;

typedef enum {
	OBJ_NONE	= 0,
	OBJ_GRAVITY	= 1<<0,		/* Object is subject to gravity */
} ObjectAttrib;

typedef struct {
	
	Model *model;
	
	GLfloat x;
	GLfloat y;
	GLfloat z;

	ObjectAttrib attribs;
	GLfloat vx;
	GLfloat vy;
	GLfloat vz;
	
	float yaw;
	float yawspeed;
	
	int landed;			/* Object has landed */
	int recharging;			/* Object has landed on a platform, so is recharging */
	
} ModelInstance;

typedef struct {
	char *name;
	GLuint texname;
	GLuint normalmap;
	int has_normals;
} Texture;

typedef struct {

	int	shaders;
	int	vbos;
	int	fbos;
	int	timer_queries;
	
	/* Lighting shaders */
	GLuint	lighting_vert;
	GLuint	lighting_frag;
	GLuint	lighting_program;
	GLuint	fill_vert;
	GLuint	fill_frag;
	GLuint	fill_program;

	/* Textures */
	Texture		textures[MAX_TEXTURES];
	unsigned int	num_textures;
	
	/* Swirlyness stuff */
	GLuint		swirly_vert;
	GLuint		swirly_frag;
	GLuint		swirly_program;
	GLuint		swirly_fbo;
        GLuint		swirly_texture;
	
	GLfloat         aspect;
	int             width;
	int             height;

} RenderContext;

#define AUDIO_MAX_SOUNDS 16
typedef struct {

	/* There are two mono samples in a stereo sample. */
	struct sound_struct {
		Sint16 *data;
		long dpos;		/* Number of (mono) samples played so far */
		long dlen;		/* Number of (mono) samples in 'data' */
		int inuse;
		int playing;
		int repeat;
		float volume;
		size_t decode_pos;	/* Number of (mono) samples decoded so far */
	} sounds[AUDIO_MAX_SOUNDS];
	
	pthread_mutex_t sounds_mutex;			/* Mutex for the list of sounds being played */
	pthread_t dispatch_threads[AUDIO_MAX_SOUNDS];	/* List of dispatch threads */
	int dispatch_threads_inuse[AUDIO_MAX_SOUNDS];	/* List of 'in use' flags for the dispatch threads */
	
	int debug;
	int startup;
	float startup_volume;
	int paused;
	int shutdown;
	
} AudioContext;

typedef struct {
	AudioContext *audiocontext;
	char *filename;
	float volume;
	int repeat;
	int idx;
} AudioDispatchData;

typedef struct {
	int rx;
	int ry;
	int rz;
} Connection;

typedef struct {
	GLfloat x;
	GLfloat y;
	GLfloat z;
} Light;

typedef struct {

	ModelInstance **objects;
	int num_objects;
	
	int rx;
	int ry;
	int rz;
	
	int needed_this_time;
	int checked_this_time;
	Connection connected[MAX_ROOMS];
	int num_connected;
	
	char *comment;
	
	Light lights[MAX_LIGHTS];
	int num_lights;
	
} Room;

typedef struct {

	int debug;

	unsigned int thrusting;
	unsigned int turn_left;
	unsigned int turn_right;
	unsigned int forward;
	unsigned int reverse;
	
	double tlast;
	double time;			/* Time in the game (milliseconds since start of game
					/ * as measured at the start of the current frame */
	
	ModelInstance *lander;
	
	ModelContext *models;
	RenderContext *render;
	AudioContext *audio;
	
	int cur_room_x;
	int cur_room_y;
	int cur_room_z;
	
	Room *rooms[MAX_ROOMS];
	int num_rooms;
	
	float view_angle;
	float view_yaw;
	float view_dist;
	int paused;
	int pause_rel;
	
	/* Performance monitoring stuff */
	int		frames;
	double		t_fps;
	int		fps;
	int		query_this_frame;
	GLuint		timer_query;
	long long int	time_render;	/* Time taken to render, in us */
	long long int	time_physics;	/* Time taken for physics, in us */
	
	GLfloat		radiation;
	GLfloat		fuel;
	
	GLfloat		platform_rel_x;
	GLfloat		platform_rel_y;
	GLfloat		time_of_landing_event;
		
} Game;

typedef struct {

	double nx;
	double ny;
	double nz;	/* Normal of face being collided with */
	
	double cx;
	double cy;
	double cz;	/* Coordinates of object at the moment when it collides */
	
	double ttc;
	
	ModelInstance *obj;	/* The object being collided against */

} CollisionSpec;

#endif	/* TYPES_H */

