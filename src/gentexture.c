/*
 * gentexture.c
 *
 * Generate normal map stuff
 *
 * Copyright (c) 2008 Thomas White <taw27@cam.ac.uk>
 *
 * This file is part of Thrust3D - a silly game
 *
 * Thrust3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Thrust3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrust3D.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <png.h>
#include <stdlib.h>
#include <stdint.h>

int main(int argc, char *argv[]) {

	FILE *fh;
	png_bytep header;
	png_structp png_ptr;
	png_infop info_ptr;
	png_infop end_info;
	unsigned int width;
	unsigned int height;
	unsigned int bit_depth;
	unsigned int channels;
	png_bytep *row_pointers;
	unsigned int x;
	unsigned int y;
	uint8_t *texels;
	char *filename;
	char *outfilename;
	
	filename = argv[1];
	outfilename = argv[2];
	
	/* Open file */
	fh = fopen(filename, "rb");
	if ( !fh ) {
		fprintf(stderr, "Couldn't open texture file '%s'\n", filename);
		return 1;
	}
	
	/* Check it's actually a PNG file */
	header = malloc(8);
	fread(header, 1, 8, fh);
	if ( png_sig_cmp(header, 0, 8)) {
		fprintf(stderr, "Texture file '%s' is not a PNG file.\n", filename);
		free(header);
		fclose(fh);
		return 1;
	}
	free(header);
	
	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if ( !png_ptr ) {
		fprintf(stderr, "Couldn't create PNG read structure.\n");
		fclose(fh);
		return 1;
	}
	
	info_ptr = png_create_info_struct(png_ptr);
	if ( !info_ptr ) {
		png_destroy_read_struct(&png_ptr, (png_infopp)NULL, (png_infopp)NULL);
		fprintf(stderr, "Couldn't create PNG info structure.\n");
		fclose(fh);
		return 1;
	}
	
	end_info = png_create_info_struct(png_ptr);
	if ( !end_info ) {
		png_destroy_read_struct(&png_ptr, &info_ptr, (png_infopp)NULL);
		printf("Couldn't create PNG end info structure.\n");
		fclose(fh);
		return 1;
	}
	
	if ( setjmp(png_jmpbuf(png_ptr)) ) {
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		fclose(fh);
		fprintf(stderr, "PNG read failed.\n");
		return 1;
	}
	
	png_init_io(png_ptr, fh);
	png_set_sig_bytes(png_ptr, 8);
	
	/* Read! */
	png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);
	
	width = png_get_image_width(png_ptr, info_ptr);
	height = png_get_image_height(png_ptr, info_ptr);
	bit_depth = png_get_bit_depth(png_ptr, info_ptr);
	channels = png_get_channels(png_ptr, info_ptr);
	if ( bit_depth != 8 ) {
		fprintf(stderr, "Texture image '%s' doesn't have 8 bits per channel per pixel.\n", filename);
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		fclose(fh);
		return 1;
	}
	if ( channels != 4 ) {
		fprintf(stderr, "Texture image '%s' doesn't have 4 channels.\n", filename);
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
		fclose(fh);
		return 1;
	}
	
	/* Get image data */
	row_pointers = png_get_rows(png_ptr, info_ptr);
	
	texels = malloc(4*width*height);
	for ( y=0; y<height; y++ ) {
		for ( x=0; x<width; x++ ) {
		
			unsigned int r, g, b, a;

			r = row_pointers[y][(channels*x)+0];
			g = row_pointers[y][(channels*x)+1];
			b = row_pointers[y][(channels*x)+2];
			a = row_pointers[y][(channels*x)+3];
			
			texels[4*(x + width*(height-1-y)) + 0] = r;
			texels[4*(x + width*(height-1-y)) + 1] = g;
			texels[4*(x + width*(height-1-y)) + 2] = b;
			texels[4*(x + width*(height-1-y)) + 3] = a;
			
		}
	}
	
	png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
	fclose(fh);
	
	fh = fopen(outfilename, "wb");
	if (!fh) {
		fprintf(stderr, "Couldn't open output file.\n");
		return 1;
	}
	png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
	if ( !png_ptr ) {
		fprintf(stderr, "Couldn't create PNG write structure.\n");
		fclose(fh);
		return 1;
	}	
	info_ptr = png_create_info_struct(png_ptr);
	if ( !info_ptr ) {
		png_destroy_write_struct(&png_ptr, (png_infopp)NULL);
		fprintf(stderr, "Couldn't create PNG info structure.\n");
		fclose(fh);
		return 1;
	}	
	if ( setjmp(png_jmpbuf(png_ptr)) ) {
		png_destroy_write_struct(&png_ptr, &info_ptr);
		fclose(fh);
		fprintf(stderr, "PNG write failed.\n");
		return 1;
	}	
	png_init_io(png_ptr, fh);
	
	png_set_IHDR(png_ptr, info_ptr, width, height, 8, PNG_COLOR_TYPE_RGBA, PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);
	
	/* Write the image data */
	row_pointers = malloc(height*sizeof(png_bytep *));

	for ( y=0; y<height; y++ ) {
		row_pointers[y] = malloc(width*4);
		for ( x=0; x<width; x++ ) {
		
			row_pointers[y][4*x+0] = texels[4*(x + width*(height-1-y)) + 0];
			row_pointers[y][4*x+1] = texels[4*(x + width*(height-1-y)) + 1];
			row_pointers[y][4*x+2] = texels[4*(x + width*(height-1-y)) + 2];
			row_pointers[y][4*x+3] = texels[4*(x + width*(height-1-y)) + 3];
			
		}
	}

	png_set_rows(png_ptr, info_ptr, row_pointers);
	png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);
	
	png_destroy_write_struct(&png_ptr, &info_ptr);
	for ( y=0; y<height; y++ ) {
		free(row_pointers[y]);
	}
	free(row_pointers);
	fclose(fh);
	
	free(texels);
	
	return 0;
	
}

