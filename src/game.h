/*
 * game.h
 *
 * Game book-keeping
 *
 * Copyright (c) 2008 Thomas White <taw27@cam.ac.uk>
 *
 * This file is part of Thrust3D - a silly game
 *
 * Thrust3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Thrust3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrust3D.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#ifndef GAME_H
#define GAME_H

#include <gl.h>

#include "types.h"

extern void game_check_handoff(Game *game);
extern Game *game_new(int width, int height, GameOptions gameopts);
extern void game_shutdown(Game *game);
extern Room *game_find_room(Game *game, int rx, int ry, int rz);
extern void game_pause(Game *game);

#endif	/* GAME_H */

