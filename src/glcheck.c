/*
 * glcheck.c
 *
 * Quick check of OpenGL functionality
 *
 * Copyright (c) 2008 Thomas White <taw27@cam.ac.uk>
 *
 * This file is part of Thrust3D - a silly game
 *
 * Thrust3D is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Thrust3D is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Thrust3D.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <glew.h>
//#define GL_GLEXT_PROTOTYPES 1
//#include <gl.h>
#include <glut.h>

GLuint buffer;
GLfloat angle = 0.0;
GLuint program;

/* Utility function to load and compile a shader, checking the info log */
static GLuint render_load_shader(const char *filename, GLenum type) {

	GLuint shader;
	char text[4096];
	size_t len;
	FILE *fh;
	int l;
	GLint status;
	
	fh = fopen(filename, "r");
	if ( fh == NULL ) {
		fprintf(stderr, "Couldn't load shader '%s'\n", filename);
		return 0;
	}
	len = fread(text, 1, 4095, fh);
	fclose(fh);
	text[len] = '\0';
	const GLchar *source = text;
	shader = glCreateShader(type);
	glShaderSource(shader, 1, &source, NULL);
	glCompileShader(shader);
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status); 
	if ( status == GL_FALSE ) {
		glGetShaderInfoLog(shader, 4095, &l, text);
		if ( l > 0 ) {
			printf("%s\n", text); fflush(stdout);
		} else {
			printf("Shader compilation failed.\n");
		}
	}
	
	return shader;
	
}

static int render_validate_shader(GLuint shader) {
	
	GLint status;
	int l;
	char text[4096];
	
	glValidateProgram(shader);
	glGetProgramiv(shader, GL_VALIDATE_STATUS, &status); 
	if ( status == GL_FALSE ) {
		glGetProgramInfoLog(shader, 4095, &l, text);
		if ( l > 0 ) {
			printf("%s\n", text); fflush(stdout);
		} else {
			printf("Shader did not validate successfully.\n");
		}
		return 0;
	}
	
	return 1;
	
}

static void glcheck_setup() {

	GLuint vert, frag;

	glGenBuffers(1, &buffer);
	glBindBuffer(GL_ARRAY_BUFFER, buffer);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	
	vert = render_load_shader(DATADIR"/shaders/lighting.vert", GL_VERTEX_SHADER);
	frag = render_load_shader(DATADIR"/shaders/lighting.frag", GL_FRAGMENT_SHADER);
	program = glCreateProgram();
	glAttachShader(program, vert);
	glAttachShader(program, frag);
	glLinkProgram(program);
	render_validate_shader(program);
	
}

static void glcheck_draw() {
	
	GLfloat white[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat black[] = { 0.0, 0.0, 0.0, 1.0 };
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(50.0, 1.0, 0.1, 100.0);
	
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0,  0.0, -2.0,
		  0.0,  0.0,  0.0,
		  0.0,  1.0,  0.0);
	
	glEnable(GL_LIGHTING);
	GLfloat ambient[] = { 0.2, 0.2, 0.2, 1.0 };
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, ambient);
	GLfloat pos0[] = { 0.0, 0.0, -2.0, 0.0 };
	GLfloat diffuse0[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat specular0[] = { 1.0, 1.0, 1.0, 1.0 };
	GLfloat ambient0[] = { 0.0, 0.0, 0.0, 0.0 };
	glLightfv(GL_LIGHT0, GL_POSITION, pos0);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse0);
	glLightfv(GL_LIGHT0, GL_SPECULAR, specular0);
	glLightfv(GL_LIGHT0, GL_AMBIENT, ambient0);
	glEnable(GL_LIGHT0);
	
	glRotatef(angle, 0.0, 1.0, 0.0);
	
	glFrontFace(GL_CW);
	glUseProgram(program);
	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT, GL_AMBIENT_AND_DIFFUSE);
	glMaterialfv(GL_FRONT, GL_SPECULAR, white);
	glMaterialfv(GL_FRONT, GL_EMISSION, black);
	glMaterialf(GL_FRONT, GL_SHININESS, 80.0);
	glColor3f(0.0, 0.0, 1.0);
	glutSolidTeapot(0.5);
	glUseProgram(0);
	glFrontFace(GL_CCW);
	
	glutSwapBuffers();

}

void glcheck_update() {
	angle += 0.3;
	glutPostRedisplay();
	glutTimerFunc(10.0, glcheck_update, 0.0);
}

int main(int argc, char *argv[]) {

	int window;
	GLint var;

	glutInit(&argc,argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	glutInitWindowSize(512, 512);
	window = glutCreateWindow("Thrust3D GLcheck");
	
	glewInit();
	glcheck_setup();
	
	glGetIntegerv(GL_MAX_FRAGMENT_UNIFORM_COMPONENTS, &var);
	printf("Max uniform components: %i\n", var);
	glGetIntegerv(GL_MAX_VARYING_FLOATS, &var);
	printf("Max varying components: %i\n", var);
	
	glutTimerFunc(10.0, glcheck_update, 0.0);
	glutDisplayFunc(glcheck_draw);
	glutMainLoop();
	
	glutDestroyWindow(window);
	
	return 0;

}

